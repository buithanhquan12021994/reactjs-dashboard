/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}",],
  darkMode: 'class',
  theme: {
    fontFamily: {
      display: ['Open Sans', 'sans-serif'],
      body: ['Open Sans', 'sans-serif']
    },
    extend: {
      fontSize: {
        14: '14px'
      },
      backgroundColor: {
        'main-bg': '#FAFBFB',
        'main-dark-bg': '#20232A',
        'secondary-dark-bg': '#33373E',
        'light-gray': '#F7F7F7',
        'half-transparent': 'rgba(0, 0, 0, 0.5)',
      },
      borderWidth: {
        1: '1px'
      },
      borderColor: {
        color: 'rgba(0, 0, 0, 0.1)'
      },
      width: {
        400: '400px',
        760: '760px',
        780: '780px',
        800: '800px',
        1000: '1000px',
        1200: '1200px',
        1400: '1400px',
      },
      height: theme => ({
        auto: 'auto',
        ...theme('spacing'),
        full: '100%',
      }),
      minHeight: theme => ({
        '0': '0',
        ...theme('spacing'),

        full: '100%',
        minHeight: 'calc(var(--vh, 1vh) * 100)'
      }),
      backgroundImage: {
        'hero-pattern': "url('/src/assets/hero.png')",
        'admin-login-background': "url('/src/assets/img/admin_login.svg')"
      }
    },
  },
  plugins: [],
}
