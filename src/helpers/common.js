import { _ } from "core-js";
import moment from 'moment';

// buildUrl with query
export function buildUrl(url, query) {
    if (!query || query === {} || Array.isArray(query)) {
      return url;
    }
    for (const keyname in query) {
      if (typeof query[keyname] == 'object') {
        continue;
      }
      const param = `{${keyname}}`;
      while (url.includes(param)) {
        url = url.replace(param, query[keyname]);
      }
    }
    const queries = [];
    for (const keyname in query) {
      if (typeof query[keyname] !== 'object') {
        queries.push(`${keyname}=${query[keyname]}`);
        continue;
      }
      if (Array.isArray(query[keyname])) {
        queries.push(`${keyname}=${query[keyname].join(',')}`);
      } else {
        queries.push(`${keyname}=${JSON.stringify(query[keyname])}`);
      }
    }
    return `${url}?${queries.join('&')}`;
  }

// convert object contants like  { foo: {id: 1, text: 'text'} } => [{ id: 1, text: 'text }] --> use this for options-select2
export const formatConstantToArray = function (object) {
  if (typeof object != 'object') {
      throw 'Parameter must be an OBJECT type!';
  }
  let array = [];
  for (const prop in object) {
      array.push({
          id: object[prop].value,
          text: object[prop].text
      });
  }
  return array;
}

export function awaitAll(promises) {
  if (!Array.isArray(promises)) throw new Error('The param should be an Array Type!');
  return new Promise((resolve, reject) => {
    Promise.all(promises).then(result => resolve(result)).catch(err =>{console.log(err); reject(err)});
  });
}


export function awaitAllsettled(listPromise) {
  if (!Array.isArray(listPromise)) throw new Error('The param should be an Array Type!');
  return Promise.allSettled(listPromise).then(result => Promise.resolve(result)).catch(err => Promise.reject(err));
}


export function findObjectRecursiveArr(array, key, value) {
  var o;
  array.some(function iter(a) {
      if (a[key] === value) {
          o = a;
          return true;
      }
      return Array.isArray(a.children) && a.children.some(iter);
  });
  return o;
}

export function formatFormRequest(objectForm) {
  let form = new FormData();

  const convertNullToEmpty = (value) => value != null ? value : "";

  for (let dataKey in objectForm) {
    if (Array.isArray(objectForm[dataKey])) {
      let dataArrayCollection = convertNullToEmpty(objectForm[dataKey]);
      if (dataArrayCollection) {
        dataArrayCollection  = JSON.stringify(dataArrayCollection);
      }

      form.append(dataKey, dataArrayCollection);
    } else if (typeof objectForm[dataKey] === 'object') {
      for (let prop in objectForm[dataKey]) {
        form.append(`${dataKey}[${prop}]`, convertNullToEmpty(objectForm[dataKey][prop]));
      }
    } else {
      form.append(dataKey, convertNullToEmpty(objectForm[dataKey]));
    }
  }
  return form;
}


export function autoDownloadFromUrl(url, fileName = null) {
  const element = document.createElement('a');
  element.setAttribute('href', url);
  if (fileName) {
    element.setAttribute('download', fileName);
  }

  element.style.display = 'none';

  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

export function convertToPrincipleFormatDateTime(dataDate, format = 'YYYY/MM/DD') {
  if (moment(dataDate).isValid()) {
    return moment(dataDate).format(format);
  }
  return '';
}

export function convertNullToEmpty(value) {
  return (value != null && value != undefined) ? value : "";
}

export const buildExtendInfoformData = ({ extend_info }) => {
  let form = new FormData();

  if (!_.isEmpty(extend_info)) {
    for (let keyData in extend_info) {

      if (Array.isArray(extend_info[keyData].value)) {
        if (extend_info[keyData].value.length) {
          extend_info[keyData].value.forEach((data, idx) => {
            form.append(`extend_info[${keyData}][value][${idx}]`, convertNullToEmpty(data));
          })
        } else {
          form.append(`extend_info[${keyData}][value]`, '');
        }

      } else if (typeof extend_info[keyData].value == 'object') {
        for (let childKey in extend_info[keyData].value) {
          form.append(`extend_info[${keyData}][value][${childKey}]`, convertNullToEmpty(extend_info[keyData].value[childKey]));
        }
      } else {
        form.append(`extend_info[${keyData}][value]`, convertNullToEmpty(extend_info[keyData].value));
      }
    }
  }

  return form;
}


export const downloadBlobFile = (blob, filename =  'test_download') => {
  const url = window.URL.createObjectURL(new Blob([blob], { type: blob.type }));
  const link = document.createElement('a');
  link.href = url;
  link.setAttribute('download', filename);
  document.body.appendChild(link);
  link.click();
  window.URL.revokeObjectURL(url);
  link.remove();
}

export const isPropExistInObject = (object, key) => Object.prototype.hasOwnProperty.call(object, key);

/* eslint-disable */
export const pipeline = (...functions) => (value) => {
  return functions.reduce(async (currentValue, currentFunction) => {
    try {
      return await currentFunction(currentValue);

    } catch (err) {

    }
  }, value);
}