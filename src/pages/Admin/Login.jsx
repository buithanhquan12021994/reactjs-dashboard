import React from "react";
import { Form, Input, Button, Checkbox, ConfigProvider } from "antd";
import AdminLoginImg from "../../assets/img/admin_login.svg";
import { StyleProvider } from "@ant-design/cssinjs";

const Login = () => {
  const handleLogin = () => {
    alert("Login data");
  };

  return (
    <ConfigProvider>
      <StyleProvider hashPriority="high">
        <div className="h-screen flex w-screen min-h-screen min-w-screen justify-center items-center">
          <div className="h-full relative md:flex hidden p-5 bg-slate-50 md:w-3/5">
            <div className="flex px-5 items-center justify-center my-auto w-full">
              <img src={AdminLoginImg} />
            </div>
          </div>
          <div className="flex w-full md:w-2/5 justify-center py-10 items-center bg-white">
            <div className="w-100 w-full sm:w-4/6 md:w-3/5 sm:p-2">
              <h1 className="text-gray-800 font-bold text-2xl mb-6">
                Balloon Admin 👋{" "}
              </h1>
              <div className="block">
                <Form
                  name="Login"
                  layout="vertical"
                  autoComplete="off"
                  onFinish={handleLogin}
                >
                  {/* username */}
                  <Form.Item
                    label="Username"
                    name="username"
                    rules={[
                      { required: true, message: "Please input your username" },
                    ]}
                  >
                    <Input placeholder="Username" />
                  </Form.Item>

                  {/* password */}
                  <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                      { required: true, message: "Please input your password" },
                    ]}
                  >
                    <Input.Password placeholder="Password" />
                  </Form.Item>

                  {/* remember */}
                  <Form.Item>
                    <Form.Item name="remember" valuePropName="checked" noStyle>
                      <Checkbox>Remeber me</Checkbox>
                    </Form.Item>

                    <a className="float-right" href="">
                      Forgot password
                    </a>
                  </Form.Item>

                  {/* button submit */}
                  <Form.Item labelCol>
                    <Button type="primary" block htmlType="submit">
                      Login
                    </Button>
                  </Form.Item>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </StyleProvider>
    </ConfigProvider>
  );
};

export default Login;
