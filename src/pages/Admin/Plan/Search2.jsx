import React from 'react'
import PlanService from 'services/admin/plan';
import SearchModule from 'modules/SearchModule';
import PlanFormSearch from 'components/Form/Admin/PlanFormSearch';
import { BaseTranslation } from 'components/Base';

function CustomeName({name}) {
  return (  
    <p className="text-blue-500">{name}</p>
  )
}

const Search2 = () => {

  const fields = [
    {
      title: <BaseTranslation keyName={"plan.label_plan_type"} />,
      dataIndex: "plan_type_name",
      key: "plan_type_name",
    },
    {
      title: <BaseTranslation keyName="plan.label_plan_type_name" />,
      dataIndex: "name",
      key: "name",
    },
    {
      title: <BaseTranslation keyName="plan.label_plan_value" />,
      dataIndex: "value",
      key: "value",
    },
  ]

  function doSomethingAfterGetResultSearching(listData, setData) {
    console.log('here');
    console.log(listData);
    let data = [...listData];
    data = data.map(item => ({
      ...item,
      name: <CustomeName name={item.name}/>
    }));
    console.log(data);
    setData(data);
  }

  return (
    <SearchModule
      category="Plan"
      title="Search"
      formSearch={<PlanFormSearch/>}
      fields={fields}
      service={PlanService}
      methodSearch="search"
      createPath="/admin/plan/create"
      editPath="/admin/plans/edit"
      callbackAfterSearch={null}
    />
  )
}

export default Search2