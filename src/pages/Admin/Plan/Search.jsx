import React, { useState, useEffect, useTransition, useReducer } from "react";
import { Card, Header } from "../../../components";
import { Form, Input, Button, Select } from "antd";
import PlanService from "../../../services/admin/plan";
import {
  useLoaderData,
  Link,
  useNavigate,
  useSearchParams,
  useLocation,
} from "react-router-dom";

import useQuery from "hooks/useQuery";

import {
  BaseTranslation,
  BaseTable,
  BaseSelection,
} from "../../../components/Base";
import { planType } from "../../../constants/data";
import { useTranslation } from "react-i18next";

export async function loader({ request }) {
  const url = new URL(request.url);
  const query = Object.fromEntries(url.searchParams.entries());
  const res = await PlanService.search(query);
  return {
    query,
    plans: res.data,
    current: res.current_page,
    perPage: res.per_page,
    total: res.total,
  };
}

const initPagination = (initialPaginationState) => initialPaginationState

const paginationReducer = (state, action) => {
  switch (action.type) {
    case "CURRENT_PAGE":
      return {
        ...state,
        current_page: action.payload,
      };

    case "TOTAL_RECORD":
      return {
        ...state,
        total: action.payload,
      };

    case "PER_PAGE":
      return {
        ...state,
        per_page: action.payload,
      };

    case "INITIAL":
      return {
        ...state,
        current_page: 1,
        total: 0,
        per_page: 10,
      };

    default:
      return state;
  }
};

const sourceInit = {
  name: "",
  plan_type: ""
}

const PlanSearch = () => {
  // ------------- STATE -----------
  const query = useQuery();
  const [currentQueryParameters, setSearchParams] = useSearchParams();
  const [loading, setLoading] = useState(false)
  const [dataSearch, setDataSearch] = useState([]);
  const [form] = Form.useForm();
  const [pagination, dispatchPagination] = useReducer(
    paginationReducer,
    {
      current_page: query.get('page') || 1,
      total: 0,
      per_page: query.get('per_page') || 10
    },
  );
  const { t } = useTranslation();

  
  const initial = () => {
    let fieldSearch = {};
    Object.keys(sourceInit).forEach(key => {
      fieldSearch[key] = query.get(key) || sourceInit[key];
    });

    form.setFieldsValue(fieldSearch);
  }

  const searchData = async function (fieldSearch) {
    try {
      setLoading(true)
      let data = await PlanService.search(fieldSearch);
      setDataSearch(data.data);
      let { current_page, total, per_page } = data;
  
      // set pagination
      dispatchPagination({ type: "CURRENT_PAGE", payload: current_page });
      dispatchPagination({ type: "TOTAL_RECORD", payload: total });
      dispatchPagination({ type: "PER_PAGE", payload: per_page });

      //
      setLoading(false);

    } catch (err) {
      setLoading(false);
    }
  };

  const changePage = (page) => {
    dispatchPagination({ type: "CURRENT_PAGE", payload: page });
  }

  const onReset = () => {
    form.resetFields();
  };

  const executeSearch = (querySearch) => {
    setSearchParams(querySearch);
    searchData(querySearch);
  }

  const startSearch = () => {
    let fieldSearch = form.getFieldsValue();
    let querySearch = {
      ...fieldSearch,
      page: pagination.current_page,
      per_page: pagination.per_page,
    };
    executeSearch(querySearch)
  };

  useEffect(() => {
    initial();
  }, [])

  useEffect(() => {
    startSearch();
  }, [pagination.current_page])


  const fields = [
    {
      title: <BaseTranslation keyName={"plan.label_plan_type"} />,
      dataIndex: "plan_type_name",
      key: "plan_type_name",
    },
    {
      title: <BaseTranslation keyName="plan.label_plan_type_name" />,
      dataIndex: "name",
      key: "name",
    },
    {
      title: <BaseTranslation keyName="plan.label_plan_value" />,
      dataIndex: "value",
      key: "value",
    },
  ];

  const layout = {
    labelCol: {
      span: 4,
    },
    wrapperCol: {
      span: 18,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };

  return (
    <>
      <Card>
        <Header category="Plan" title="Search" />
        <div className="flex justify-center items-center">
          <Form
            {...layout}
            form={form}
            labelWrap
            autoComplete="off"
            className="w-1/2"
            onFinish={startSearch}
          >
            {/* Plan type */}
            <Form.Item
              name="plan_type"
              label={t("plan.label_plan_type")}
              className="font-semibold"
              labelWrap
              initialValue=""
            >
              <Select
                showSearch
                placeholder={t("common.label_please_choose")}
                allowClear={true}
                options={Object.values(planType)}
              />
            </Form.Item>

            {/* Plan name */}
            <Form.Item
              name="name"
              label="契約種別名"
              className="justify-center font-semibold"
              initialValue=""
            >
              <Input />
            </Form.Item>

            <Form.Item {...tailLayout}>
              <Button className="m-2" type="primary" htmlType="submit">
                Search
              </Button>
              <Button className="m-2" htmlType="button" onClick={onReset}>
                Reset
              </Button>
            </Form.Item>
          </Form>
        </div>
      </Card>

      <div className="m-2 md:mx-7 my-3 flex justify-end">
        <Link to="/admin/plans/create">
          <Button type="primary" htmlType="button">
            {t("common.label_create")}
          </Button>
        </Link>
      </div>

      <Card>
        <BaseTable
          fields={fields}
          data={dataSearch}
          loading={loading}
          current={pagination.current_page}
          perPage={pagination.per_page}
          total={pagination.total}
          editPathRecord="/admin/plans/edit"
          changePage={changePage}
        />
      </Card>
    </>
  );
};

export default PlanSearch;
