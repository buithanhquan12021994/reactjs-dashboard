import React, { useState } from "react";
import { Card, Header } from "../../../components";
import { Form, Spin } from "antd";
import { useTranslation } from "react-i18next";
import PlanForm from "../../../components/Form/Admin/PlanForm";
import PlanService from "services/admin/plan";

const PlanCreate = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();

  const layout = {
    labelCol: {
      span: 4,
    },
    wrapperCol: {
      span: 18,
    },
  };
  const tailLayout = {
    wrapperCol: {
      span: 24,
    },
  };

  const onSubmit = async (fieldsValue) => {
    console.log(fieldsValue);
    setLoading(true);

    try {
      await PlanService.store(fieldsValue);
      setLoading(false);
    } catch (e) {
      console.log(e)
    }
  };

  return (
    <>
      <Card>
        <Header category="Plan" title="Create" />
        <Spin spinning={loading}>
          <div className="flex justify-center items-center">
            <Form
              {...layout}
              form={form}
              labelWrap
              autoComplete="off"
              className="w-full"
              onFinish={onSubmit}
            >
              <PlanForm />

              <Form.Item {...tailLayout}>
                <div className="flex w-full justify-between">
                  <button className="bg-slate-400 hover:bg-slate-300  text-white font-bold p-2 text-center rounded">
                    {t("common.label_go_back")}
                  </button>
                  <button className="bg-blue-600 hover:bg-blue-500 text-white font-bold p-2 text-center rounded">
                    {t("common.label_register")}
                  </button>
                </div>
              </Form.Item>
            </Form>
          </div>
        </Spin>
      </Card>
    </>
  );
};

export default PlanCreate;
