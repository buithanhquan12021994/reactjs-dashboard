import React from "react";
import { BaseInputGroup } from "components/Base";
import { Form, Button, Input } from "antd";
import { Card } from "components";
const BaseInputForm = () => {
  const [form] = Form.useForm();

  const handleSubmit = (fields) => {
    console.log(fields);
  };
  return (
    <Card>
      <Form
        name="formtest"
        form={form}
        onFinish={handleSubmit}
        autoComplete="off"
      >
        <BaseInputGroup label="Input 1" name="input" />
        <BaseInputGroup label="Textarea" name="textarea" inputType="textarea"  />
        <BaseInputGroup label="Password" name="password" inputType="password" />
        <BaseInputGroup label="Number" name="number" inputType="number" />

        <Form.Item>
        <Button type="primary" htmlType="submit">
        Submit
      </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};

export default BaseInputForm;
