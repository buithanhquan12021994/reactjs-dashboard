import React from 'react'
import { Outlet, Link } from 'react-router-dom';
import { CrudContextProvider } from 'contexts/ContextCrud';

const PlanLayout = () => {
  return (
    <CrudContextProvider>
      <div className="flex w-full my-2 p-4 gap-5">
        <Link to="/admin/plans">
          <button className="text-sm rounded p-2 text-white bg-blue-600 hover:bg-blue-500">Search</button>
        </Link>
        <Link to="/admin/plans/search-testing">
          <button className="text-sm rounded p-2 text-white bg-blue-600 hover:bg-blue-500">Component search</button>
        </Link>
      </div>
      <Outlet/>
    </CrudContextProvider>
  )
}

export default PlanLayout