import React, { useEffect, useState } from "react";
import PlanForm from "components/Form/Admin/PlanForm";
import { Form, Spin, Alert } from "antd";
import { Card, Header } from "components";
import { useTranslation } from "react-i18next";
import PlanService from "services/admin/plan";
import { useLoaderData } from "react-router-dom";
import { defer, Await } from "react-router-dom";

export async function loader({ request, params }) {
  const url = new URL(request.url);
  const query = Object.fromEntries(url.searchParams.entries());
  let data = {};
  try {
    data = await PlanService.show(params.id);
    return {
      data,
      query,
      params,
    };
  } catch (err) {
    if (err.notFound) {
      throw new Response("Record is not found", {
        status: 404,
        statusText: "Record is not found",
      });
    }
  }
}

const PlanEdit = () => {
  const { data, query, params } = useLoaderData();
  const [loading, setLoading] = useState(false);
  const { t } = useTranslation();
  const [form] = Form.useForm();

  form.setFieldsValue({
    ...data,
  });

  const layout = {
    labelCol: {
      span: 4,
    },
    wrapperCol: {
      span: 18,
    },
  };
  const tailLayout = {
    wrapperCol: {
      span: 24,
    },
  };

  const onSubmit = async (fieldsValue) => {
    setLoading(true);
    try {
      await PlanService.update(params.id, fieldsValue)
      setLoading(false)
    } catch (err) {
      throw new Response("Can not update Record", {
        status: 500,
        statusText: "Can not update Record"
      })
    }
  };

  useEffect(() => {
    // setTimeout(() => {
    //   // form.setFieldsValue({
    //   //   plan_type: '1',
    //   //   name: 'test',
    //   //   value: 123
    //   // })
    // }, 5000);
  }, []);

  return (
    <>
      <Card>
        <Header category="Plan" title="Edit" />

        <Spin spinning={loading}>
          <div className="flex justify-center items-center">
            <Form
              {...layout}
              form={form}
              labelWrap
              autoComplete="off"
              className="w-full"
              onFinish={onSubmit}
            >
              <PlanForm />
              <Form.Item {...tailLayout}>
                <div className="flex w-full justify-between">
                  <button className="bg-slate-400 hover:bg-slate-300  text-white font-bold p-2 text-center rounded">
                    {t("common.label_go_back")}
                  </button>
                  <button className="bg-blue-600 hover:bg-blue-500 text-white font-bold p-2 text-center rounded">
                    {t("common.label_update")}
                  </button>
                </div>
              </Form.Item>
            </Form>
          </div>
        </Spin>
      </Card>
    </>
  );
};

export default PlanEdit;
