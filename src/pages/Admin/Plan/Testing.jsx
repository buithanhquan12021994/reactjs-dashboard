import React from 'react'
import { useCrudContext } from 'contexts/ContextCrud';
import CrudModules from 'modules/CrudModules';
import PlanForm from 'components/Form/Admin/PlanForm';
import PlanService from 'services/admin/plan';

const Testing = () => {
  const {  setAlert } = useCrudContext();

  const config = {
    category: "Plan",
    title: "Edit"
  }

  return (
    <>  
      <CrudModules
        category="Plan"
        title="Create"
        formElement={<PlanForm/>}
        create={true}
        service={PlanService}
        
      />
    </>
  )
}

export default Testing