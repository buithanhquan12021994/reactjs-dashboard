import React from 'react'
import { Header, Card } from '../components'

const ColorPicker = () => {
  return (
    <Card>
      <Header category="App" title="Color Picker"></Header>
      Color picker
    </Card>
  )
}

export default ColorPicker