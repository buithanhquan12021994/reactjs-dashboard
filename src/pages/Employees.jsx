import React from 'react'
import { Card, Header } from '../components';

const Employees = () => {
  return (
    <Card>
      <Header category='Page' title="Employees"/>
      Employees
    </Card>
  )
}

export default Employees