import React from 'react'
import { Card, Header} from '../components'

const Calendar = () => {
  return (
    <Card>
      <Header category="App" title="Calendar"></Header>
      Calendar
    </Card>
  )
}

export default Calendar