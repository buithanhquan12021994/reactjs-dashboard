import React from "react";
import { useRouteError } from "react-router-dom";

const NotFound = () => {
  const error = useRouteError();
  console.log(error);
  return (
    <section className="h-screen w-screen flex items-center p-16 dark:bg-gray-900 dark:text-gray-100">
      <div className="container flex flex-col items-center justify-center px-5 mx-auto my-8">
        <div className="max-w-md text-center">
          <h2 className="mb-8 font-extrabold text-9xl dark:text-gray-600">
            <span className="sr-only">Error</span>
            {error.status }
          </h2>
          <p className="text-2xl font-semibold md:text-3xl">
            { error.status === 404 && ("Sorry, we couldn't find this page.")}
          </p>
          <p className="mt-4 mb-8 dark:text-gray-400">
            { error.status === 404 && error.internal ? "But dont worry, you can find plenty of other things on our homepage." : ""}
            { error.status && !error.internal ? error.statusText : ''}
          </p>
          <a
            rel="noopener noreferrer"
            href="#"
            className="px-8 py-3 font-semibold rounded bg-blue-600 text-white hover:text-white hover:bg-blue-800"
          >
            <span className="hover:text-white">Back to homepage</span>
          </a>
        </div>
      </div>
    </section>
  );
};

export default NotFound;
