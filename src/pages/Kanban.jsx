import React from 'react'
import { Card, Header } from '../components';

const Kanban = () => {
  return (
    <Card>
      <Header category="Apps" title="Kanban"></Header>
      Kanban
    </Card>
  )
}

export default Kanban