import React from 'react'
import { Card, Header } from '../components';

const Editor = () => {
  return (
    <Card>
      <Header category="App" title="Editor"></Header>
      Editor
    </Card>
  )
}

export default Editor