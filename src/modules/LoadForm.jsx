import React, { useState } from 'react'
import { Form } from 'antd';
import { useTranslation } from 'react-i18next';
import { useCrudContext } from 'contexts/ContextCrud';
import Loading from 'components/Loading';

const LoadForm = ({ formElement, create = false, update = false, service = {}, options = {} }) => {
  const { t } = useTranslation();
  const {
    createMethod = 'store',
    updateMethod = 'update',
    deleteMethod = 'delete'
  } = options;

  const { form, modeSuccess } = useCrudContext();

  const [loading, setLoading] = useState(false);
 
  const layout = {
    labelCol: {
      span: 4,
    },
    wrapperCol: {
      span: 18,
    },
  };
  const tailLayout = {
    wrapperCol: {
      span: 24,
    },
  };

  // create new
  const createRecord = async (form) => {
    if (!Object.prototype.hasOwnProperty.call(service, createMethod)) {
      throw new Error(`Your service is supplied that [${service}] did not implement [${createMethod}!!!`);
    }
    setLoading(true);
    try {
      await service[createMethod](form);
      setLoading(false);
      modeSuccess('create')
    } catch (err) {
      setLoading(false);
    }
  }

  // update record
  const updateRecord = async (id, form) => {}
  
  // event
  const onSubmit= (field) => {
    if (create) {
      createRecord(field);
      return;
    }

    if (update) {
      updateRecord(field)
      return;
    }
  }

  return (
    <>  
      <Loading isLoading={loading}>
      <Form
        {...layout}
        form={form}
        labelWrap
        autoComplete="off"
        className="w-full"
        onFinish={onSubmit}
      >
        {/* load form */}
        { formElement }
        {/* action */}
        <Form.Item {...tailLayout}>
          <div className="flex w-full justify-between">
            <button className="bg-slate-400 hover:bg-slate-300  text-white font-bold p-2 text-center rounded">
              {t("common.label_go_back")}
            </button>
            <button className="bg-blue-600 hover:bg-blue-500 text-white font-bold p-2 text-center rounded">
              {create && t("common.label_register")}
              {update && t("common.label_update")}
            </button>
          </div>
        </Form.Item>
      </Form>
      </Loading>
    </>
  )
}

export default LoadForm