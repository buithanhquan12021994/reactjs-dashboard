import React, { useReducer, useState, useEffect } from "react";
import { Form, Button  } from "antd";
import { Card, Header } from "components";
import useQuery from "hooks/useQuery";
import { useSearchParams, Link } from 'react-router-dom';
import { useTranslation } from "react-i18next";
import { BaseTable } from "components/Base";

const paginationReducer = (state, action) => {
  switch (action.type) {
    case "CURRENT_PAGE":
      return {
        ...state,
        current_page: action.payload,
      };

    case "TOTAL_RECORD":
      return {
        ...state,
        total: action.payload,
      };

    case "PER_PAGE":
      return {
        ...state,
        per_page: action.payload,
      };

    case "INITIAL":
      return {
        ...state,
        current_page: 1,
        total: 0,
        per_page: 10,
      };

    default:
      return state;
  }
};

const SearchModule = ({
  category = "Category page",
  title = "Title page",
  formSearch,
  fields,
  service,
  methodSearch = 'search',
  createPath,
  editPath,
  initFieldSearch = {},
  layout = {
    labelCol: {
      span: 4,
    },
    wrapperCol: {
      span: 18,
    },
  },
  tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  },
  callbackAfterSearch = null,
}) => {
  // ----------------- STATE -----------------
  const { t } = useTranslation();
  const query = useQuery();
  const [loading, setLoading] = useState(false);
  const [currentQueryParameters, setSearchParams] = useSearchParams();
  const [dataSearch, setDataSearch] = useState([]);
  const [pagination, dispatchPagination] = useReducer(paginationReducer, {
    current_page: query.get("page") || 1,
    total: 0,
    per_page: query.get("per_page") || 10,
  });

  const [form] = Form.useForm();

  const initial = () => {
    let fieldSearch = {};
    Object.keys(initFieldSearch).forEach((key) => {
      fieldSearch[key] = query.get(key) || initFieldSearch[key];
    });

    form.setFieldsValue(fieldSearch);
  };

  const searchData = async function (fieldSearch) {
    try {
      setLoading(true);
      let data = await service[methodSearch](fieldSearch);
      if (callbackAfterSearch) {
        callbackAfterSearch(data.data, setDataSearch)
      } else {
        setDataSearch(data.data);
      }
      let { current_page, total, per_page } = data;

      // set pagination
      dispatchPagination({ type: "CURRENT_PAGE", payload: current_page });
      dispatchPagination({ type: "TOTAL_RECORD", payload: total });
      dispatchPagination({ type: "PER_PAGE", payload: per_page });

      //
      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  };

  const changePage = (page) => {
    dispatchPagination({ type: "CURRENT_PAGE", payload: page });
  };

  const onReset = () => {
    form.resetFields();
  };

  const executeSearch = (querySearch) => {
    setSearchParams(querySearch);
    searchData(querySearch);
  };

  const startSearch = () => {
    let fieldSearch = form.getFieldsValue();
    let querySearch = {
      ...fieldSearch,
      page: pagination.current_page,
      per_page: pagination.per_page,
    };
    executeSearch(querySearch);
  };

  // --------------- HOOKS --------------
  useEffect(() => {
    initial();
  }, [])

  useEffect(() => {
    startSearch();
  }, [pagination.current_page])


  return (
    <>
      <Card>
        <Header category={category} title={title}/>

        <div className="flex justify-center items-center">
          <Form
            {...layout}
            form={form}
            labelWrap
            autoComplete="off"
            className="w-1/2"
            onFinish={startSearch}
          >
            {formSearch}

            <Form.Item {...tailLayout}>
              <Button className="m-2" type="primary" htmlType="submit">
                Search
              </Button>
              <Button className="m-2" htmlType="button" onClick={onReset}>
                Reset
              </Button>
            </Form.Item>
          </Form>
        </div>
      </Card>

      <div className="m-2 md:mx-7 my-3 flex justify-end">
        <Link to={createPath}>
          <Button type="primary" htmlType="button">
            {t("common.label_create")}
          </Button>
        </Link>
      </div>

      <Card>
        <BaseTable
          fields={fields}
          data={dataSearch}
          loading={loading}
          current={pagination.current_page}
          perPage={pagination.per_page}
          total={pagination.total}
          editPathRecord={editPath}
          changePage={changePage}
        />
      </Card>
    </>
  );
};

export default SearchModule;
