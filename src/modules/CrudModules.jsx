import React, { useEffect } from "react";
import { Card, Header } from "components";
import { Alert } from "antd";
import LoadForm from "./LoadForm";
import { useCrudContext } from "contexts/ContextCrud";
import { useParams } from "react-router-dom";

const CrudModules = ({
  formElement,
  category,
  title,
  create = false,
  update = false,
  service = {},
  options = {
    createMethod: "store",
    updateMethod: "update",
    deleteMethod: 'delete'
  },
}) => {
  const { alert, setAlert } = useCrudContext();

  useEffect(() => {
    if (alert.show) {
      setTimeout(() => {
        setAlert({
          show: false,
          message: "",
          description: "",
          type: "",
        });
      }, 3000);
    }
  }, [alert]);


  return (
    <Card>
      <Header category={category} title={title}></Header>

      {alert.show && (
        <div className="my-2">
          <Alert
            message={alert.message}
            description={alert.description}
            type={alert.type}
            showIcon
          />
        </div>
      )}

      <LoadForm
        formElement={formElement}
        create={create}
        update={update}
        service={service}
        options={options}
      />
    </Card>
  );
};

export default CrudModules;
