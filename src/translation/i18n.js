import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

function loadLocaleMessages () {
  let messages = {};
  const locales = require.context('../locales', true, /[A-Za-z0-9-_,\s]+\.json$/i)
  locales.keys().forEach(key => {
    const domain = key.split('/')[1];
    messages[domain] = {
      translation: {}
    }
  });

  locales.keys().forEach(key => {
    const domain = key.split('/')[1];
    const matched = key.match(/([A-Za-z0-9-_]+)\./i)
    if (matched && matched.length > 1) {
      const locale = matched[1]
      // messages[domain] = {
      //   ...messages[domain],
      //   [locale]: locales(key)
      // }
      messages[domain].translation = {
        ...messages[domain].translation,
        [locale]: locales(key)
      }
    };
  });
  return messages
}

const resources = loadLocaleMessages();
console.log(resources);
i18n
  .use(initReactI18next)
  .init({
    resources: resources,
    fallbackLng: 'ja',
    // debug: true,
    interpolation: {
      escapeValue: true
    }
  });

export default i18n;