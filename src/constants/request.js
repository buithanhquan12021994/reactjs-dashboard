export default {
  defaultRequestTimeOut: 60000,
  contentType: 'application/json',
  accept: 'application/json',
}