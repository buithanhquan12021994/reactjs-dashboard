import { Translation } from "react-i18next";
export const statusData = {
  active: {
    value: 1,
    label: (
      <Translation>{(t) => <span>{t("content.active")}</span>}</Translation>
    ),
  },
  notActive: {
    value: 2,
    label: (
      <Translation>{(t) => <span>{t("content.inActive")}</span>}</Translation>
    ),
  },
};

export const languages = {
  en: {
    value: "en",
    label: "English",
  },
  ja: {
    value: "ja",
    label: "Japanese",
  },
};

export const planType = {
  registry: {
    value: 1,
    label: (
      <Translation>{(t) => <span>{t("plan.label_member_registry_number")}</span>}</Translation>
    ),
  },
  active: {
    value: 2,
    label: (
      <Translation>{(t) => <span>{t("plan.label_member_active_number")}</span>}</Translation>
    )
  },
  organization: {
    value: 3,
    label: (
      <Translation>{(t) => <span>{t("plan.label_organization_number")}</span>}</Translation>
    )
  },
};
