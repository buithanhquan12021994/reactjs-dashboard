import React from 'react'
import { Outlet } from 'react-router-dom';
import { useStateContext } from '../contexts/ContextProvider'
import { Navbar, ThemeSettings } from '../components';

const TheAdminContent = () => {
  const { activeMenu, themeSettings } = useStateContext();

  return (
    <div
      className={`dark:bg-main-dark-bg bg-slate-100 min-h-screen w-full ${
        activeMenu ? "md:ml-72" : ""
      }`}
    >
      <div className="fixed md:static bg-slate-100 dark:bg-main-dark-bg navbar w-full">
        <Navbar />
      </div>

      {/* Content Routing Content */}
      <div>
        {themeSettings && <ThemeSettings />}
        
        <Outlet/>
      </div>
    </div>
  )
}

export default TheAdminContent