import React from 'react'
import { FiSettings } from 'react-icons/fi';
import { Sidebar } from '../components';
import { useStateContext } from '../contexts/ContextProvider'
import TheAdminContent from './TheAdminContent';
import { Tooltip } from 'antd';

const TheAdminLayouts = () => {
  const { activeMenu, setThemeSettings, currentColor, currentMode } = useStateContext();

  return (
    <div className={currentMode === "Dark" ? "dark" : ""}>
      <div className="flex relative dark:bg-main-dark-bg">
        <div className="fixed right-4 bottom-4" style={{ zIndex: "1000" }}>
          <Tooltip title="Settings" placement="top">
            <button
              type="button"
              className="text-3xl p-3 hover:drop-shadow-xl hover:bg-light-gray text-white"
              style={{ background: currentColor, borderRadius: "50%" }}
              onClick={() => setThemeSettings(true)}
            >
              <FiSettings />
            </button>
          </Tooltip>
        </div>

        {/* active sidebar menu */}
        {activeMenu && (
          <div className="md:w-72 sx:w-70 fixed sidebar dark:bg-secondary-dark-bg bg-white">
            <Sidebar />
          </div>
        )}

        {/* content page */}
        <TheAdminContent/>
      </div>
    </div>
  )
}

export default TheAdminLayouts