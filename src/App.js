import React, { useEffect} from 'react'
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './App.css';
import routes from './routes';

const App = () => {
  let router = createBrowserRouter(routes);

  useEffect(() => {
  }, [])
  return (
    <>
      <RouterProvider router={router}/>
    </>
  );
}

export default App