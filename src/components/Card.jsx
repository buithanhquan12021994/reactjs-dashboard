import React from 'react'

const Card = ({ children }) => {
  return (
    <div className="m-2 md:m-7 p-2 md:p-7 bg-white rounded-3xl">
      {children}
    </div>
  )
}

export default Card