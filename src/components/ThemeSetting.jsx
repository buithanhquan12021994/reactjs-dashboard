import React, { useState } from "react";
import { MdOutlineCancel } from "react-icons/md";
import { BsCheck } from "react-icons/bs";
import { themeColors } from "../data/dummy";
import { useStateContext } from "../contexts/ContextProvider";
import { Tooltip } from 'antd';
import { languages, statusData } from "../constants/data";

const ThemeSetting = () => {
  const { setColor, setMode, currentColor, currentMode, setThemeSettings, currentLanguage, setLanguage } = useStateContext();

  return (
    <div className="bg-half-transparent w-screen fixed nav-item top-0 right-0">
      <div className="float-right h-screen dark:text-gray-200 dark:bg-secondary-dark-bg bg-white dark:[#484B52] w-400">
        <div className="flex justify-between items-center p-4 ml-4">
          <p className="font-semibold text-xl">Settings</p>
          <button
            type="button"
            onClick={() => setThemeSettings(false)}
            style={{ color: "rgb(153, 171, 180)", borderRadius: "50%" }}
            className="text-2xl p-3 hover:drop-shadow-sm hover:bg-light-gray"
          >
            <MdOutlineCancel />
          </button>
        </div>

        {/* Language  */}
        <div className="flex-col border-t-1 border-color p-4 ml-4">

          <p className="font-semibold text-lg">Languages</p>
          {/* Japanese */}
          { Object.values(languages).map(item => (
            <div className="mt-4" key={item.value}>
              <input 
                type="radio"
                id={item.value}
                name="language"
                value={item.value}
                onChange={(e) => setLanguage(e.target.value)}
                checked={item.value === currentLanguage}
              />

              <label htmlFor={item.value} className="ml-2 text-md cursor-pointer">
                {item.label}
              </label>
            </div>
          ))}
        </div>
    
        {/* Theme setting  */}
        <div className="flex-col border-t-1 border-color p-4 ml-4">
          <p className="font-semibold text-lg">Theme Options</p>
          {/* Light */}
          <div className="mt-4">
            <input
              type="radio"
              id="light"
              name="theme"
              value="Light"
              className="cursor-pointer"
              onChange={() => setMode('Light')}
              checked={currentMode === 'Light'}
            />
            <label htmlFor="light" className="ml-2 text-md cursor-pointer">
              Light
            </label>
          </div>

          {/* Dark */}
          <div className="mt-4">
            <input
              type="radio"
              id="dark"
              name="theme"
              value="Dark"
              className="cursor-pointer"
              onChange={() => setMode('Dark')}
              checked={currentMode === 'Dark'}
            />
            <label htmlFor="dark" className="ml-2 text-md cursor-pointer">
              Dark
            </label>
          </div>
        </div>

        {/* Theme color */}
        <div className="flex-col border-t-1 border-color p-4 ml-4">
          <p className="font-semibold text-lg">Theme color</p>
          <div className="flex gap-3">
            {themeColors.map((item, index) => (
              <Tooltip key={index} title={item.name} placement="top" zIndex="9999">
                <div className="relative mt-2 cursor-pointer flex gap-5 items-center">
                  <button
                    type="button"
                    className="h-10 w-10 rounded-full cursor-pointer"
                    style={{ backgroundColor: item.color }}
                    onClick={() => setColor(item.color)}
                  >
                    <BsCheck
                      className={`ml-2 text-2xl text-white ${
                        item.color === currentColor ? "block" : "hidden"
                      }`}
                    />
                  </button>
                </div>
              </Tooltip>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ThemeSetting;
