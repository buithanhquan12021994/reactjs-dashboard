import React from "react";
import { Form, Select, Input } from "antd";
import { useTranslation } from "react-i18next";
import { planType } from "constants/data";

const PlanFormSearch = () => {
  const { t } = useTranslation();

  return (
    <>
        {/* Plan type */}
        <Form.Item
          name="plan_type"
          label={t("plan.label_plan_type")}
          className="font-semibold"
          labelWrap
          initialValue=""
        >
          <Select
            showSearch
            placeholder={t("common.label_please_choose")}
            allowClear={true}
            options={Object.values(planType)}
          />
        </Form.Item>

        {/* Plan name */}
        <Form.Item
          name="name"
          label="契約種別名"
          className="justify-center font-semibold"
          initialValue=""
        >
          <Input />
        </Form.Item>
    </>
  );
};

export default PlanFormSearch;
