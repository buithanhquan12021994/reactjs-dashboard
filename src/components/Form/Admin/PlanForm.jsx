import React from "react";
import { Form, Select, Input, InputNumber } from 'antd';
import { planType } from "../../../constants/data";
import { useTranslation } from 'react-i18next';

const PlanForm = () => {
  const { t } = useTranslation();
  return (
    <>
      {/* plan type */}
      <Form.Item
        name="plan_type"
        label="Plan Type"
        rules={[
          {
            required: true,
            message: "Please select a plan type",
          },
        ]}
        labelWrap
      >
        <Select
          showSearch
          placeholder={t("common.label_please_choose")}
          allowClear={true}
          options={Object.values(planType)}
        />
      </Form.Item>

      {/* plan name */}
      <Form.Item
        name="name"
        label="Plan Name"
        rules={[
          {
            required: true,
            message: "Please type name",
          },
        ]}
        labelWrap
      >
        <Input />
      </Form.Item>

      {/* plan name */}
      <Form.Item
        name="value"
        label="Plan Value"
        rules={[
          {
            required: true,
            message: "Please type value",
          },
        ]}
        labelWrap
      >
        <InputNumber style={{ width: "100%" }} />
      </Form.Item>
    </>
  );
};

export default PlanForm;
