import React from "react";
import { Form, Input, InputNumber } from "antd";
import { useTranslation } from "react-i18next";
// provide: text, number,

const LoadInputComponent = ({
  inputType,
  disabled = false,
  options = [],
  configInput = {},
  ...props
}) => {
  const { t } = useTranslation();

  switch (inputType) {
    case "text":
      return (
        <Input
          placeholder={t("common.label_placeholder")}
          disabled={disabled}
          {...configInput}
          {...props}
        />
      );

    case "textarea":
      return (
        <Input.TextArea
          placeholder={t("common.label_placeholder")}
          disabled={disabled}
          {...configInput}
          {...props}
        />
      );

    case "password":
      return (
        <Input.Password
          placeholder={t("common.label_placeholder")}
          disabled={disabled}
          {...configInput}
          {...props}
        />
      );

    case "number":
      return (
        <InputNumber
          placeholder={t("common.label_placeholder")}
          disabled={disabled}
          {...configInput}
          {...props}
        />
      );

    default:
      return null;
  }
};

const BaseInputGroup = ({
  label,
  name,
  rules,
  children,
  inputType = "text",
  disabled = false,
  options = [],
  configInput = {},
  configFormItem = {
    initialValue: ''
  },
}) => {

  const { t } = useTranslation();

  return (
    <Form.Item label={label} name={name} rules={rules} {...configFormItem}>
      <LoadInputComponent inputType={inputType} disabled={disabled} options={options} configInput={configInput}/>
    </Form.Item>
  );
};

export default BaseInputGroup;


export {
  LoadInputComponent
}