import React, { Children } from 'react'

const BaseButton = ({children, ...props}) => {
  return (
    <Button {...props}>
      {children}
    </Button>
  )
}

export default BaseButton