import React from 'react'
import { Select } from 'antd';
import { useTranslation } from 'react-i18next';

const BaseSelection = ({ options, handleChange,  name, ...props }) => {
  const { t } = useTranslation();

  const triggerChange = (e) => {
    if (typeof e !== 'undefined') {
      if (handleChange) {
        handleChange(e);
      }
      return;
    }

  }

  const triggerClear = () => {
    if  (handleChange) {
      handleChange('');
    } 
  }

  return (
    <div className="block">
      <Select
        {...props}
        name={name}
        showSearch
        placeholder={t('common.label_please_choose')}
        allowClear={true}
        options={options}
        onChange={triggerChange}
        onClear={triggerClear}
      />
    </div>
  )
}

export default BaseSelection