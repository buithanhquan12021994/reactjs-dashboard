import React from "react";
import { Translation } from "react-i18next";

const BaseTranslation = ({ keyName, children }) => {
  return (
    <Translation>
      {(t) => <>{children ? children : <>{t(`${keyName}`)}</>}</>}
    </Translation>
  );
};

export default BaseTranslation;
