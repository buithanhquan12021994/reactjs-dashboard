import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top'
    },
    title: {
      display: true,
    },
  },
  updateMode: 'resize'
};


const SparkLineChart = ({ currentColor }) => {
  const data = {
    labels: ['January', 'February', 'March', 'April',],
    datasets: [
      {
        fill: false,
        label: 'Report',
        data: [ 
         10, 50, 1,28
        ],
        borderColor: currentColor,
      },
    ],
  }

  return <Line options={options} data={data}/>
}


export default SparkLineChart