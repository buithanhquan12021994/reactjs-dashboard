import {
  Home,
  Products,
  Calendar,
  Employees,
  Customers,
  Kanban,
  ColorPicker,
  Editor,
  Login
} from "../../../pages";
import TheAdminLayouts from "../../../layouts/TheAdminLayouts";
import plan from './plan';
import { NotFound, Forbidden } from "pages/Errors";
import { Suspense } from "react";

const admin = [
  {
    path: "/admin/login",
    element: <Login/>
  },
  {
    path: "/admin",
    element: <Suspense fallback={<p>Loading................</p>}><TheAdminLayouts /></Suspense>,
    errorElement: <NotFound/>,
    children: [
      {
        index: true,
        element: <Home />,
      },
      // routing here
      plan,
      {
        path: "home",
        element: <Home />,
      },
      {
        path: "products",
        element: <Products />,
      },
      {
        path: "employees",
        element: <Employees />,
      },
      {
        path: "customers",
        element: <Customers />,
      },
      {
        path: "calendar",
        element: <Calendar />,
      },
      {
        path: "kanban",
        element: <Kanban />,
      },
      {
        path: "color-picker",
        element: <ColorPicker />,
      },
      {
        path: "editor",
        element: <Editor />,
      },
    ],
  }
]


export default admin;