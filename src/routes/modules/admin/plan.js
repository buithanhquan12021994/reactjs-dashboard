import { PlanSearch, PlanCreate, PlanEdit, PlanLayout, Testing, PlanSearchTesting, BaseInputForm } from "../../../pages/Admin/Plan";
import { loader as rootLoader } from '../../../pages/Admin/Plan/Search';
import { loader as getDetail } from 'pages/Admin/Plan/Edit';
import { CrudContextProvider } from 'contexts/ContextCrud';


const planRoute = {
  path: "plans",
  element: <PlanLayout />,
  children: [
    {
      index: true,
      element: <PlanSearch />,
      // loader: rootLoader,
    },
    {
      path: "",
      element: <PlanSearch />
    },
    { 
      path: "search-testing",
      element: <PlanSearchTesting/>
    },
    {
      path: "create",
      element: <PlanCreate />,
    },
    {
      path: "edit/:id",
      element: <PlanEdit />,
      loader: getDetail
    },
    {
      path: "testing",
      element: <CrudContextProvider><Testing/></CrudContextProvider>
    },
    {
      path: 'input-component',
      element: <BaseInputForm/>
    }
  ],
};

export default planRoute;
