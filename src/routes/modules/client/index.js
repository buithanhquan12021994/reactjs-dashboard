import TheClientLayout from "../../../layouts/TheClientLayout";
import { NotFound, Forbidden } from "../../../pages/Errors";
import { Suspense } from "react";

const client = [
  {
    path: "/forbidden",
    element: <Forbidden />,
  },
  {
    path: "/",
    element: (
      <Suspense fallback={<p>Loading</p>}>
        <TheClientLayout />
      </Suspense>
    ),
    errorElement: <NotFound />,
  },
];

export default client;
