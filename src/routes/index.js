import admin from './modules/admin';
import client from './modules/client';

const routes = [
  ...admin,
  ...client,
]

export default routes;