import {  commonService } from '../../api/services';
const prefix = '/admin/plans'

export default {
  search: (query = {}) => commonService.get(`${prefix}`, query),
  store: (planForm) => commonService.post(`${prefix}`, {}, planForm),
  show: (planId) => commonService.get(`${prefix}/${planId}`, {}),
  update: (planId, planForm) => commonService.put(`${prefix}/${planId}`, {}, planForm),
}