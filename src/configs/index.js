const loadConfig = (name = '', defaultValue = '') => {
  return process.env[name] || defaultValue;
}

const config = {
  appName: loadConfig('REACT_APP_NAME', 'app Name'),
  apiUrl: loadConfig('REACT_APP_API_URL', 'http://localhost/api')
}

export {
  loadConfig
};
export default config;