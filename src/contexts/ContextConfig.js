import React, { useContext, createContext } from 'react';

import config from '../configs';

const ConfigContext = createContext();

export const ConfigProvider = ({children}) => {
  return (
    <ConfigContext.Provider
      value={{
        ...config
      }}
    >
      {children}
    </ConfigContext.Provider>
  );
};

export const useConfigContext = () => useContext(ConfigContext);