import React, { useState, createContext, useContext } from "react";
import { Form } from 'antd';
import { useTranslation } from "react-i18next";

const CrudContext = createContext();

export const CrudContextProvider = ({ children }) => {
  const { t } = useTranslation();

  const [form] = Form.useForm();

  const [loading, setLoading] = useState(false);

  const [alert, setAlert] = useState({
    show: false,
    message: "asdasd",
    description: "",
    type: "",
  });

  // mode alert
  const modeSuccess = (mode) => {
    let alert = {
      show: true,
      message: "",
      description: "",
      type: "success"
    }

    switch (mode) {
      case 'create':
        alert = {
          ...alert,
          description: t('common.label_create_success'),
          message: t('common.label_success')
        }
        break;
      
      case 'update':
        alert = {
          ...alert,
          description: t('common.label_update_success'),
          message: t('common.label_success')
        }
        break;
    
      case 'delete':
        
        alert = {
          ...alert,
          description: t('common.label_delete_success'),
          message: t('common.label_success')
        }
        break;
      default:
        break;
    }
    setAlert(alert)
  }

  return (
    <CrudContext.Provider
      value={{
        loading,
        setLoading,
        setAlert,
        alert,
        form,
        modeSuccess
      }}
    >
      {children}
    </CrudContext.Provider>
  );
};

export const useCrudContext = () => useContext(CrudContext);
