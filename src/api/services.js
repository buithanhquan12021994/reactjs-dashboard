import config from '../configs';
import instanceService from './instanceService';

export const commonService = new instanceService(config.apiUrl);