import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { ContextProvider } from './contexts/ContextProvider';
import { ConfigProvider } from './contexts/ContextConfig';
import i18n from './translation/i18n';
import { I18nextProvider } from 'react-i18next';
import "./index.css";
import { ConfigProvider as ConfigAntdProvider } from "antd";
import { StyleProvider } from "@ant-design/cssinjs";
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ConfigAntdProvider>
      <StyleProvider hashPriority="high">
        <ConfigProvider>
          <ContextProvider>
            <I18nextProvider i18n={i18n}>
              <App />
            </I18nextProvider>
          </ContextProvider>
        </ConfigProvider>
      </StyleProvider>
    </ConfigAntdProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals